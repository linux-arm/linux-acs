/*
 * BSA SBSA ACS Platform module.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2016-2019, 2021, 2023, 2024, Arm Limited
 *
 * Author: Sakar Arora<sakar.arora@arm.com>
 *
 */

#include <linux/acpi_iort.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/version.h>
#include <linux/acpi.h>

#include "common/include/pal_linux.h"
#include "sbsa/include/sbsa_pal_linux.h"
#include "val/sbsa/include/sbsa_pal_interface.h"

#define NOT_IMPLEMENTED  0x4B1D

/* UUID for PCIe */
static const guid_t pci_guid =
       GUID_INIT(0xE5C937D0, 0x3553, 0x4D7A,
                 0x91, 0x17, 0xEA, 0x4D, 0x19, 0xC3, 0x43, 0x4D);

/* Defines for the _DSM method for obtaining Steering tag value */
#define PCI_REVISION     0x6
#define PCI_FUNC_INDEX   0x0E

acpi_status ste_user_function(acpi_handle handle, uint32_t level, void *context, void **return_value);


/**
    @brief   Gets RP support of transaction forwarding.

    @param   bus        PCI bus address
    @param   dev        PCI device address
    @param   fn         PCI function number
    @param   seg        PCI segment number

    @return  0 if rp not involved in transaction forwarding
             1 if rp is involved in transaction forwarding
**/
uint32_t
pal_pcie_get_rp_transaction_frwd_support(uint32_t seg, uint32_t bus, uint32_t dev, uint32_t fn)
{
  return 1;
}



/**
  @brief  This function is called by the function pal_pcie_dsm_ste_tags
          when the any device is found

  @param  handle       device handle
  @param  level        nesting level
  @param  context      data to be passed from caller function
  @param  return_value return value passed to caller function pal_pcie_dsm_ste_tags

  @return 0 - Failure, NOT_IMPLEMENTED - DSM Method not implemented, Other values - PASS
**/
acpi_status ste_user_function(acpi_handle handle, uint32_t level,
                   void *context, void **return_value)
{

  acpi_status status;
  struct acpi_buffer buffer;
  union acpi_object *obj;
  uint32_t ste_value;

  buffer.length = ACPI_ALLOCATE_BUFFER;
  status = acpi_get_name(handle, ACPI_FULL_PATHNAME, &buffer);

  if (acpi_has_method(handle, "_DSM")) {
      obj = acpi_evaluate_dsm_typed(handle, &pci_guid, PCI_REVISION,
                                    PCI_FUNC_INDEX, NULL, ACPI_TYPE_INTEGER);

      if (!obj) {
          acs_print(ACS_PRINT_INFO,"\n DSM method for STE not present.", 0);
          return NOT_IMPLEMENTED;
      }

      ste_value = obj->integer.value;
      acs_print(ACS_PRINT_DEBUG,"\n Steering tag value is %x", ste_value);
      if (ste_value == 0) {
          ACPI_FREE(obj);
          return ste_value;
      }

      ACPI_FREE(obj);
      return ste_value;
  }
  else
      return 0;

  return NOT_IMPLEMENTED;
}

/**
  @brief  Check for the _DSM method to obtain the STE value

  @param  None

  @return 0 - Failure, NOT_IMPLEMENTED - DSM Method not implemented, Other values - PASS
**/
uint32_t pal_pcie_dsm_ste_tags(void)
{
    acpi_handle handle = NULL;
    acpi_status status;

    status = acpi_get_devices(NULL, ste_user_function, NULL, &handle);
    return status;
}